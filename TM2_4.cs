using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM2_4

//Create a new class called “RandomHelper” which contains the following:  
// 1. A static method called RandInt that accepts two integers and 
// returns a random integer between them. Make sure that the numbers are inclusive
//  (i.e. if you call RandInt(1,10)
//   you should be able to generate random integer including 1 and 10  
// 2. A static method called RandDouble that accepts two integers and returns a random double between them.  
// 3. Call the methods from another class without instantiating the class (i.e. call it just like
//  you would call  Math.Abs(-20) since your methods are defined as static) 
// Hint: Use Random class to generate random numbers  
 
{
    
   public class Program{
	static public void Main (){
		//Code
        Console.WriteLine(RandomHelper.RandInt(1,5));
		Console.WriteLine(RandomHelper.RandDouble(1,5));

}
}
class RandomHelper{
        public static int RandInt(int a,int b){
            Random rnd = new Random();
            return rnd.Next(a-1,b+1);    
        }
        public static double RandDouble(int rangeMin,int rangeMax){
           Random r = new Random();
            return  rangeMin + (rangeMax - rangeMin) * r.NextDouble();
        }

    }

}
