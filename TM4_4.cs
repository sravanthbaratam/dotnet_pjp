using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM4_4
{
    class Program
    {
    public static void strtoalphadigits(string text,ArrayList alpha,ArrayList digits)
{
   for (var i = 0; i < text.Length - 1; i++)
    {
        if ((char.IsLetter(text[i])))
        {
        if(!alpha.Contains(text[i]))
            alpha.Add(text[i]);
        }
        else if((char.IsDigit(text[i]))){
            if(!digits.Contains(text[i]))
            digits.Add(text[i]);
        }
    }
    alpha.Sort();
    digits.Sort();
}
        static void Main(string[] args)
        {
            var text = Console.ReadLine();
            ArrayList alpha = new ArrayList();
            ArrayList digits = new ArrayList();
            strtoalphadigits(text,alpha,digits);
            foreach(var v in alpha)
            Console.WriteLine(v);
      
            foreach(var v in digits)
            Console.WriteLine(v);
        }
    }
}
