﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            int alp, digit, i, l;
            alp = digit = i = 0;

            str = Console.ReadLine();
            l = str.Length;

            /* Checks each character of string*/

            while (i < l)
            {
                if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z'))
                {
                    alp++;
                }
                else if (str[i] >= '0' && str[i] <= '9')
                {
                    digit++;
                }               
                i++;
            }

            Console.Write("Number of Alphabets in the string is : {0}\n", alp);
            Console.Write("Number of Digits in the string is : {0}\n", digit);
        }
    }
}
   

       
