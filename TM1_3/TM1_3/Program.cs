﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM1_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create a console application that accepts a string and increments each character in the string by 1
            string str;
            str = Console.ReadLine();
            int l = str.Length;
            char[] a = new char[l];
            for (int i = 0; i < str.Length; i++)
            {
                a[i] = str[i];
                if (a[i] == 'z')
                {
                    a[i] = 'a';
                }
                else
                    if (a[i] == 'Z')
                {
                    a[i] = 'A';
                }
                else
                a[i] = ++a[i];
            }
            Console.WriteLine(a);

        }
    }
}
