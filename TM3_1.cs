using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace TM3_1
{   

 public class InvalidNameException : Exception
        {         
           public InvalidNameException():base()
            {
            }   
            public InvalidNameException (string message): base(String.Format("its a negative number :{0}",message))
            {

            }
        }
    class Program
    {
        static void Main(string[] args)
        {
            DateTime inputtedDate = DateTime.Parse(Console.ReadLine());
            Person  emp =new Person("srvanth","baratan","baratams@gmial.com",inputtedDate);
            Console.WriteLine(emp.zodiac_sign());
        }
    }

    class Person{
         public string FirstName,LastName,EmailAddress;
         public DateTime DateofBirth;
         public int day;
         public int month;

         public Person(){
             //class-constructure for derived for class 
         }
         public Person(string firstname,string lastname,string emailAddress,DateTime dateofbirth){
             if(firstname==null)
             throw new NullReferenceException(firstname);
             if(lastname==null)
             throw new NullReferenceException(lastname);
             Regex regex = new Regex("^[a-zA-Z]+$");
             if(!regex.IsMatch(firstname))
             throw new InvalidNameException(firstname);
             if(!regex.IsMatch(lastname))
             throw new InvalidNameException(lastname);

             FirstName = firstname;
             LastName = lastname;
             EmailAddress = emailAddress;
             DateofBirth = dateofbirth;
             day = DateofBirth.Day;
             month = DateofBirth.Month;
         }
         public bool IsAdult(){
             TimeSpan ts = DateTime.Now.Subtract(DateofBirth);
            int years = ts.Days / 365;
            if(years>18)
            return true;
            else
            return false;

        }
        public string zodiac_sign() 
    { 
        string astro_sign=""; 
          
        // checks month and date within the  
        // valid range of a specified zodiac 
        if (month == 12){ 
              
            if (day < 22) 
            astro_sign = "Sagittarius"; 
            else
            astro_sign ="capricorn"; 
        } 
              
        else if (month == 1){ 
            if (day < 20) 
            astro_sign = "Capricorn"; 
            else
            astro_sign = "aquarius"; 
        } 
              
        else if (month == 2){ 
            if (day < 19) 
            astro_sign = "Aquarius"; 
            else
            astro_sign = "pisces"; 
        } 
              
        else if(month == 3){ 
            if (day < 21)  
            astro_sign = "Pisces"; 
            else
            astro_sign = "aries"; 
        } 
        else if (month == 4){ 
            if (day < 20) 
            astro_sign = "Aries"; 
            else
            astro_sign = "taurus"; 
        } 
              
        else if (month == 5){ 
            if (day < 21) 
            astro_sign = "Taurus"; 
            else
            astro_sign = "gemini"; 
        } 
              
        else if( month == 6){ 
            if (day < 21) 
            astro_sign = "Gemini"; 
            else
            astro_sign = "cancer"; 
        } 
              
        else if (month == 7){ 
            if (day < 23) 
            astro_sign = "Cancer"; 
            else
            astro_sign = "leo"; 
        } 
              
        else if( month == 8){ 
            if (day < 23)  
            astro_sign = "Leo"; 
            else
            astro_sign = "virgo"; 
        } 
              
        else if (month == 9){ 
            if (day < 23) 
            astro_sign = "Virgo"; 
            else
            astro_sign = "libra"; 
        } 
              
        else if (month == 10){ 
            if (day < 23) 
            astro_sign = "Libra"; 
            else
            astro_sign = "scorpio"; 
        } 
              
        else if (month == 11){ 
            if (day < 22) 
            astro_sign = "scorpio"; 
            else
            astro_sign = "sagittarius"; 
        } 
              
        return astro_sign; 
    } 
        //  public string SunSign(DateTime x){
        //     string returnString = string.Empty;
        //     // DateofBirth ????????????
            
        //     string[] dateAndMonth = x.ToLongDateString().Split(new char[] { ',' });
        //     string[] ckhString = dateAndMonth[1].ToString().Split(new char[] { ' ' });
        //     if (ckhString[1].ToString() == "March")
        //     {
        //         if (Convert.ToInt32(ckhString[2]) <= 20)
        //         {
        //             returnString = "Pisces";
        //         }
        //         else
        //         {
        //             returnString = "Aries";
        //         }
        //     }
        //     else if (ckhString[1].ToString() == "April")
        //     {
        //         if (Convert.ToInt32(ckhString[2]) <= 19)
        //         {
        //             returnString = "Aries";
        //         }
        //         else
        //         {
        //             returnString = "Taurus";
        //         }
        //     }
        //     return returnString;
        // }
          public bool IsBirthDay(){
            if ((this.DateofBirth.Day == DateTime.Now.Day) && (this.DateofBirth.Month == DateTime.Now.Month))
                {
                    return true;
                }
                else
                {
                    return false;
                }        }
         public string ScreenName(){
            return FirstName+LastName+DateofBirth.Date+DateofBirth.Year;
        }


    }

     public interface IPayable {
        int CalculatePay();
    }

    class Employee : Person,IPayable {
        double Salary;
        public int CalculatePay(){
            return 0;
        } 
    }
    class HourlyEmployee : Person,IPayable{
        double HoursWorked;
        double PayPerHour;
         public int CalculatePay(){
             return 0;
         } 
    }
    class PermanentEmployee : Person{
        double HRA,DA,Tax,NetPay,TotalPay;
        public PermanentEmployee(double hra,double da,double tax){
            HRA=hra;
            DA=da;
            Tax = tax;
        }
        
    }
   


}
