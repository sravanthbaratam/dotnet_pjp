﻿using System;
namespace TM1_8
//Write a program that accepts 2 numbers.And prints whether the 2nd number is in units, ten's, hundreds or thousands place in 1st number.  
//Control 
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = Convert.ToInt32(Console.ReadLine());
            int num2 = Convert.ToInt32(Console.ReadLine());
            if (num1 % 10 == num2)
            {
                Console.WriteLine("2nd number is in units in 1st number ");
            }
            if ((num1 / 10) % 10 == num2)
            {
                Console.WriteLine("2nd number is in tens's in 1st number ");
            }
            if ((num1 / 100) % 10 == num2)
            {
                Console.WriteLine("2nd number is in hundreds in 1st number ");
            }
            if((num1 / 1000) % 10 == num2)
            {
                Console.WriteLine("2nd number is in thousands in 1st number ");
            }
        }
    }
}