using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace demo
{
     public class NegativeNumberException : Exception
        {         
           public NegativeNumberException():base()
            {
            }   
            public NegativeNumberException (int message): base(String.Format("its a negative number :{0}",message))
            {

            }
        }
    
    class Program
    {
        static void Main(string[] args)
        {
            try{
                string name =Console.ReadLine();
                int a = int.Parse(Console.ReadLine());
                if(a<0)
                throw new NegativeNumberException(a);
                 int b = int.Parse(Console.ReadLine());
                   if(b<0)
                 throw new NegativeNumberException(b);
                  int c = int.Parse(Console.ReadLine());
                   if(c<0)
                 throw new NegativeNumberException(c);
            }
            catch(FormatException fex){
                Console.WriteLine(fex.Message);
            }
            catch(NegativeNumberException nnp){
                Console.WriteLine(nnp.Message);
            }
            finally{

            }
        
        }
    }
}
