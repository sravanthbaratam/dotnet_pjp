using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM2_3
 //Create a class called “Stock” which contains the following:  
 /// 1. StockName as string  
//2. StockSymbol as string 
//3. PreviousClosingPrice as double 
// 4. CurrentClosingPrice as double  
//5. A Constructor that allows you to define a stock’s name, symbol, previousclosingprice and currentclosingprice 
// 6. A method GetChangePercentage() that returns the percentage change from the PreviousClosingPrice to the CurrentClosingPrice  
 

{
    class Program
    {
        static void Main(string[] args)
        {
        }

    }

    class Stock {
        string StockName;
        string StockSymbol;
        double previousclosingprice;
        double currentclosingprice;

        public Stock(string stockname,string stocksymbol,double Previousclosingprice,double Currentclosingprice){
            StockName = stockname;
            StockSymbol = stocksymbol;
            previousclosingprice = Previousclosingprice;
            currentclosingprice = Currentclosingprice;
        }
        public GetChangePercentage(){
            Console.WriteLine((currentclosingprice-previousclosingprice)*100/previousclosingprice);
        }
    }
}
