using System;
namespace TM1_11
//Write a program to accept 10 integers to an array and reverse the array also print it.  
{
public class Program{
	static public void Main (){
		//Code
        int[] arr = new int[10];
        for(int i=0;i<10;i++){
            arr[i] = Convert.ToInt32(Console.ReadLine());
        }
        Array.Reverse(arr);
        for(int i=0;i<10;i++){
        Console.WriteLine(arr[i]);
        }
	}
}
}