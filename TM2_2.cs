using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM2_2
//  Create an Employee class with the following specifications.
 // 1. EmployeeName as string. 
   //2. BasicSalary, HRA, DA, TAX, GrossSalary and NetSalary as double. 
    //3. Calculate the HRA (15% of BasicSalary), DA (10% of BasicSalary), GrossSalary (BasicSalary + HRA + DA), Tax (8% of GrossSalary) and NetSalary (GrossSalary – Tax). 
     //4. A Constructor to define the EmployeeName and BasicSalary.  
     //5. A method CalculateNetPay to calculate the HRA, DA, Tax, GrossSalary and NetSalary values using the criteria mentioned in the Point 3.  
     //6. A method Display to Display the Salary structure.   
{
    class Program
    {
        static void Main(string[] args)
        {
        }

    class Employee{
        string EmployeeName;
        double BasicSalary,HRA,DA, TAX, GrossSalary, NetSalary; 
        public Employee(string name,int salary){
            EmployeeName = name;
            BasicSalary = salary;
        }
        public CalculateNetPay(){
            HRA = BasicSalary*.15;
            DA = BasicSalary*.1;
            GrossSalary = BasicSalary+HRA+DA;
            TAX = GrossSalary*.08;
            NetSalary = GrossSalary-TAX;

        }
        public DisplaySalaryStructure(){
            Console.WriteLine("Salary_structure");
            Console.WriteLine("hra : {0} \n da: {1}\n grosssalary: {2}\n tax: {3}\n netsalary:{4} ",HRA,DA,GrossSalary,TAX,NetSalary);
        }
    }
    }
}
