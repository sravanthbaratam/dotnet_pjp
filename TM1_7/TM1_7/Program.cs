﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM1_7
    //Create a console application to check whether the entered string is a Palindrome or not Control
{
    class Program
    {
        static void Main(string[] args)
        {
            string s, revs = "";
            Console.WriteLine(" Enter string");
            s = Console.ReadLine();
            for (int i = s.Length - 1; i >= 0; i--) //**i want to replace this for loop**
            {
                revs += s[i].ToString();
            }
            if (revs == s) // Checking whether string is palindrome or not
            {
                Console.WriteLine("String is Palindrome");
            }
            else
            {
                Console.WriteLine("String is not Palindrome ");
            }
          
        }
    }
}
