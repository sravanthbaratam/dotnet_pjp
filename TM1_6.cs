﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM1_6
//Write a simple program that accepts two number num1 and num2.Assign num1 to num2 in two scenarios  
//1) Assign num1 to num2 by pre-incrementing num1 and observe the output  
//    2)Assign num1 to num2 by post-incrementing num1 and observe the output 3) Swap both values
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = Convert.ToInt32(Console.ReadLine());
            int num2= Convert.ToInt32(Console.ReadLine());
            int opr = Convert.ToInt32(Console.ReadLine());
            if(opr == 1)
            {
                num2 = ++num1;
                Console.WriteLine("num1 :{0} num2: {1}", num1, num2);
            }
            else if(opr == 2)
            {
                num2 = num1++;
                Console.WriteLine("num1 :{0} num2: {1}", num1, num2);
            }

        }
    }
}
