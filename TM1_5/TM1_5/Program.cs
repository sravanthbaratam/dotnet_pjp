﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM1_5

//    Create a console application to accept a string and perform below string manipulation on the same 1) Print the string in reverse order
//2) Extract part of the string from 2nd position till the end of the string
//3) Replace any given character by '$' and print the new string
//4) Copy the string to another string variable, Modify the data in 2nd string variable and print both the strings
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                string s = Console.ReadLine();
                // four operations
                int opr = Convert.ToInt32(Console.ReadLine());
                
                if (opr == 1)
                {
                    char[] arr = s.ToCharArray();
                    Array.Reverse(arr);
                    Console.WriteLine( new string(arr));
                    break;
                }
                else
                if (opr ==2)
                {
                    string sub = s.Substring(1, s.Length);
                    Console.WriteLine("Substring: {0}", sub);
                    break;
                }
                else if (opr ==3 )
                {
                    char[] arr = s.ToCharArray();
                    arr[0] = '$';
                    Console.WriteLine(new string(arr));
                    break;
                }
                else if (opr ==4)
                {
                    string s1 = s;
                    char[] arr = s1.ToCharArray();
                    Array.Reverse(arr);
                    Console.WriteLine(s);
                    Console.WriteLine(new string(arr));
                    break;
                }
                else
                {
                    break;
                }
            }
        }
    }
}
