﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace TM01
{
    class Program
    {
        static void Main(string[] args)
        {
            int testcases = int.Parse(Console.ReadLine());
            if (testcases > 0)
            {
                while (testcases > 0)
                {
                    string[] input = Console.ReadLine().Split(' ');
                    string inputstring = input[0];
                    int firstindex = int.Parse(input[1]);
                    int lastindex = int.Parse(input[2]);
                    char[] String = inputstring.ToCharArray();
                    Array.Sort(String, firstindex, lastindex - firstindex + 1);
                    Array.Reverse(String, firstindex, lastindex - firstindex + 1);
                    Console.WriteLine(String);
                    testcases--;
                }
            }
            Console.ReadLine();
        }
    }
}