using System;
using System.Collections; 
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee("asdf",1,10000);
            Employee d = new Employee("bsa",2,10000);
            EmployeeDAL empdal = new EmployeeDAL();
            empdal.AddEmployee(e);
            empdal.AddEmployee(d);
            empdal.DeleteEmployee(1);
            
             foreach (Employee i in empdal.GetAllEmployeesistAll())
        {
            Console.WriteLine(i.EmployeeName);
        }
        }

    }
    class Employee {
        public Employee(   string employeeName,int employeeID,double salary)
        {
            EmployeeName = employeeName;
            EmployeeID = employeeID;
            Salary = salary;
        }
        public string EmployeeName;
        public int EmployeeID;
        public double Salary;

    }
    class EmployeeDAL
    {
        ArrayList EmployeeList = new ArrayList();

        public void AddEmployee(Employee e){
            EmployeeList.Add(e);
        } 
        public bool DeleteEmployee(int id) {

           foreach(Employee e in EmployeeList){
               if(e.EmployeeID == id)
               {      
                   EmployeeList.Remove(e);
                   return true;
               }
           }
           return false;
            
        }
        public bool SearchEmployee(int id){
           foreach(Employee e in EmployeeList){
               if(e.EmployeeID == id)
               {      
                   return true;
               }
           }
           return false;
        }
        public ArrayList GetAllEmployeesistAll(){
            return EmployeeList;
        }
    }

    


}
