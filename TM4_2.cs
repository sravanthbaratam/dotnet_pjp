using System;
using System.Collections; 
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee("asdf",1,10000);
            Employee d = new Employee("bsa",2,10000);
            EmployeeDAL empdal = new EmployeeDAL();
            empdal.AddEmployee(e);
            empdal.AddEmployee(d);
            empdal.DeleteEmployee(1);

            IList myKeyList = empdal.EmployeeList.GetKeyList();
            IList myValueList = empdal.EmployeeList.GetValueList();
            Console.WriteLine( "\t-KEY-\t-VALUE-" );
            for ( int i = 0; i < empdal.Count; i++ )
                Console.WriteLine( "\t{0}\t{1}", myKeyList[i], myValueList[i] );
        }

    }
    class Employee {
        public Employee(   string employeeName,int employeeID,double salary)
        {
            EmployeeName = employeeName;
            EmployeeID = employeeID;
            Salary = salary;
        }
        public string EmployeeName;
        public int EmployeeID;
        public double Salary;

    }
    class EmployeeDAL
    {
        public SortedList EmployeeList = new SortedList();

        public void AddEmployee(Employee e){
            EmployeeList.Add(e.EmployeeID,e);
        } 
        public bool DeleteEmployee(int id) {
                    if(EmployeeList.Contains(id)){
                   EmployeeList.Remove(id);
                   return true;}
                   return false;
            
        }
        public bool SearchEmployee(int id){
             if(EmployeeList.Contains(id))
                   return true;
                else 
                   return false;
        }
        public SortedList GetAllEmployeesistAll(){
            return EmployeeList;
        }
    }

    


}
