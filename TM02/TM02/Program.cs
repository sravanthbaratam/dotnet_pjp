﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TM02
{
    class Program
    {
        static void Main(string[] args)
        {
            Theatre m = new Theatre("jalsa","trivikram","pk",160);
            Console.WriteLine(m.TheatreID);

        }
    }
    class Movie
    {
         int MovieID { get; set; }
         string MovieName { get; set; }
        string Director { get; set; }
        string Producer { get; set; }
        string cast { get; set; }
        double Duration { get; set; }
        string Story { get; set; }
        string Type { get; set; }
        public Movie(string moviename, string director, string producer, string Cast, double duration, string story, string type)
        {   if(MovieName == null)
            MovieName = moviename;
        if(Director == null)
            Director = director;
        if(Producer == null)
                Producer = producer;
        if(cast==null)
            cast = Cast;
        if(Duration == 0)
            Duration = duration;
        if(Story == null)
            Story = story;
        if(Type == null)
            Type = type;

            Random rnd = new Random();
            int randomNumber = rnd.Next();
            if (MovieID == 0)
                MovieID = randomNumber;
        }
        public void DisplayMovieDetails()
        {
            Console.WriteLine("MovieID: {0}",MovieID);
            Console.WriteLine("MovieName: {0}",MovieName);
            Console.WriteLine("Director: {0}",Director);
            Console.WriteLine("Producer: {0}",Producer);
            Console.WriteLine("cast: {0}",cast);
            Console.WriteLine("Duration: {0}",Duration);
            Console.WriteLine("Story: {0}",Story);
            Console.WriteLine("Type: {0}",Type);
        }
    }
    class Theatre
    {
        public int TheatreID { get; set; }
        public string TheatreName;
        string CityName;
        string Address;
        int NumberOfScreens;
        List<Screen> Screens = new List<Screen>();
        public Theatre(string theatrename, string cityname, string address, int numberofscreens)
        {
            if (TheatreName == null)
                TheatreName = theatrename;
            if (CityName == null)
                CityName = cityname;
            if (Address == null)
                Address = address;
            if (NumberOfScreens == 0)
                NumberOfScreens = numberofscreens;
            Random rnd = new Random();
            int randomNumber = rnd.Next();
            if (TheatreID == 0)
                TheatreID = randomNumber;

            for(int i=0;i<NumberOfScreens;i++)
            {
                Screens.Add(new Screen());
            }


        }
        public void DisplayTheatreDetails()
        {
            Console.WriteLine("TheatreID: {0}",TheatreID);
            Console.WriteLine("TheatreName: {0}",TheatreName);
            Console.WriteLine("CityName: {0}",CityName);
            Console.WriteLine("Address: {0}",Address);
            Console.WriteLine("NumberOfScreens: {0}",NumberOfScreens);
            Console.WriteLine("Screens: {0}",Screens);
        }

    }
    class Screen
    {
        int ScreenID;
        SortedList<int, string> Seats = new SortedList<int, string>();
        public Screen()
        {
            ScreenID = 1000;
            for (int i = 1; i <= 50; i++)
            {
                Seats.Add(i, "Vacant");
            }

        }
    }
    class Show
    {
        int ShowID;
        int MovieID;
        int TheatreID;
        int ScreenID;
        DateTime StartDate;
        DateTime EndDate;
        decimal PlatinumSeatRate;
        decimal GoldSeatRate;
        decimal SilverSeatRate;
        public Show(int movieID, int theatreID, int screenID, DateTime startDate, DateTime endDate, decimal platinumSeatRate, decimal goldSeatRate, decimal silverSeatRate)
        {
            if (MovieID == 0)
                MovieID = movieID;
            if (TheatreID == 0)
                TheatreID = theatreID;
            if (ScreenID == 0)
                ScreenID = screenID;
            if (StartDate == null)
                StartDate = startDate;
            if (EndDate == null)
                EndDate = endDate;
            if (PlatinumSeatRate == 0)
                PlatinumSeatRate = platinumSeatRate;
            if (GoldSeatRate == 0)
                GoldSeatRate = goldSeatRate;
            if (SilverSeatRate == 0)
                SilverSeatRate = silverSeatRate;

            Random rnd = new Random();
            int randomNumber = rnd.Next();
            if (ShowID == 0)
                ShowID = randomNumber;
        }
        public void DisplayShowDetails()
        {
            Console.WriteLine("ShowID: {0}",ShowID);
            Console.WriteLine("MovieID: {0}",MovieID);
            Console.WriteLine("TheatreID: {0}",TheatreID);
            Console.WriteLine("ScreenID: {0}",ScreenID);
            Console.WriteLine("StartDate: {0}",StartDate);
            Console.WriteLine("EndDate: {0}",EndDate);
            Console.WriteLine("PlatinumSeatRate: {0}",PlatinumSeatRate);
            Console.WriteLine("GoldSeatRate: {0}",GoldSeatRate);
            Console.WriteLine("SilverSeatRate: {0}",SilverSeatRate);
        }
    }
    class User
    {
        string UserName;
        string Password;
        string UserType;
        public User(string userName, string password, string userType)
        {
            if (UserName == null)
                UserName = userName;
            if (Password == null)
                Password = password;
            if (UserType == null)
                UserType = userType;

        }

    }

    class Booking 
    {
        int BookingID;
        DateTime BookignDate;
        int ShowID;
        string CustomerName;
        int NumberOfSeats;
        string SeatType;
        decimal Amount;
        string Email;
        string BookingStatus;
        List<int> SeatNumbers;
        public Booking(int showID, string customerName, int numberOfSeats, string seatType, string email)
        {
            //Theatre t;
            //t.DisplayTheatreDetails();
            Random rnd = new Random();
            BookingID = rnd.Next(999);

            if (!(numberOfSeats > 0 || numberOfSeats < 5))
                NumberOfSeats = numberOfSeats;
            if(SeatType == null)
            {
                SeatType = seatType;
            }
            if(SeatType == "Platinum")
            {
                Amount = NumberOfSeats * 300;
            }
            if (SeatType == "Gold")
            {
                Amount = NumberOfSeats * 200;
            }
            if (SeatType == "Silver")
            {
                Amount = NumberOfSeats * 100;
            }


            BookignDate = DateTime.Now;
        }
    }

}
