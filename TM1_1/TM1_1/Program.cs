﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            double side;
            side = Convert.ToDouble(Console.ReadLine());
            double area = side * side;
            Console.WriteLine(area);
        }
    }
}
